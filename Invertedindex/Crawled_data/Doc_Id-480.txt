OrientDB - Update Record

https://www.tutorialspoint.com/orientdb/orientdb_update_record.htm

OrientDB, Tutorial, Learning, Beginners, Overview, Installation, Basic Concepts, Data Types, Console Modes, Create Database, Alter Database, Backup Database, Restore Database, Connect Database, Disconnect Database, Info Database, List Database, Freeze Database, Release Database, Config Database, Export Database, Import Database, Commit Database, Rollback Database, Optimize Database, Drop Database, Insert Record, Display Records, Load Record, Reload Record, Export Record, Update Record, Truncate Record, Delete Record, Create Class, Alter Class, Truncate Class, Drop Class, Create Cluster, Alter Cluster, Truncate Cluster, Drop Cluster, Create Property, Alter Property, Drop Property, Create Vertex, Move Vertex, Delete Vertex, Create Edge, Update Edge, Delete Edge, Functions, Sequences, Indexes, Transactions, Hooks, Caching, Logging, Performance Tuning, Upgrading, Security, Studio, Java Interface, Python Interface.

August-15-2017-02:34:10

Update Record command is used to modify the value of a particular record. SET is the basic command to update a particular field value. The following statement is the basic syntax of the Update command. 
UPDATE <class>|cluster:<cluster>|<recordID> 
   [SET|INCREMENT|ADD|REMOVE|PUT <field-name> = <field-value>[,]*] |[CONTENT| MERGE <JSON>] 
   [UPSERT] 
   [RETURN <returning> [<returning-expression>]] 
   [WHERE <conditions>] 
   [LOCK default|record] 
   [LIMIT <max-records>] [TIMEOUT <timeout>] 
 Following are the details about the options in the above syntax. SET − Defines the field to update. INCREMENT − Increments the specified field value by the given value. ADD − Adds the new item in the collection fields. REMOVE − Removes an item from the collection field. PUT − Puts an entry into map field. CONTENT − Replaces the record content with JSON document content. MERGE − Merges the record content with a JSON document. LOCK − Specifies how to lock the records between load and update. We have two options to specify Default and Record. UPSERT − Updates a record if it exists or inserts a new record if it doesn’t. It helps in executing a single query in the place of executing two queries. RETURN − Specifies an expression to return instead of the number of records. LIMIT − Defines the maximum number of records to update. TIMEOUT − Defines the time you want to allow the update run before it times out. Example Let us consider the same Customer table that we have used in the previous chapter. Sr.No. Name Age 1 Satish 25 2 Krishna 26 3 Kiran 29 4 Javeed 21 5 Raja 29 Try the following query to update the age of a customer ‘Raja’. 
Orientdb {db = demo}> UPDATE Customer SET age = 28 WHERE name = 'Raja'
 If the above query is executed successfully, you will get the following output. 
Updated 1 record(s) in 0.008000 sec(s).
 To check the record of Customer table you can use the following query. 
orientdb {db = demo}> SELECT FROM Customer 
 If the above query is executed successfully, you will get the following output. 
----+-----+--------+----+-------+---- 
#   |@RID |@CLASS  |id  |name   |age  
----+-----+--------+----+-------+---- 
0   |#11:0|Customer|1   |satish |25   
1   |#11:1|Customer|2   |krishna|26   
2   |#11:2|Customer|3   |kiran  |29 
3   |#11:3|Customer|4   |javeed |21 
4   |#11:4|Customer|5   |raja   |28   
----+-----+--------+----+-------+----
