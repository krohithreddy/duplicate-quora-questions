PostgreSQL - MAX Function

https://www.tutorialspoint.com/postgresql/postgresql_max_function.htm

PostgreSQL, Tutorial, Programming, Database, Overview, Environment Setup, Syntax, Data Type, Create, Select, Drop Database, Drop, Create Table, Schema, Insert, Select, Update, Delete Query, Operators, Expressions, Where, Like, Limit, Order By, Group By, With, Having Clause, AND and OR Operators, Distinct Keyword, Constraints, Joins, Unions Clause, NULL Values, Alias Syntax, Triggers, Indexes, Alter Table Command, Truncate Table Command, Views, Transactions, Locks, Sub Queries, Auto Increment, Privileges, DATE/TIME Functions and Operators, Functions, Useful Functions, C/C++, JAVA, PHP, Perl, Python Interface.

August-15-2017-02:34:22

PostgreSQL MAX function is used to find out the record with maximum value among a record set. To understand the MAX function, consider the table COMPANY having records as follows − 
testdb# select * from COMPANY;
 id | name  | age | address   | salary
----+-------+-----+-----------+--------
  1 | Paul  |  32 | California|  20000
  2 | Allen |  25 | Texas     |  15000
  3 | Teddy |  23 | Norway    |  20000
  4 | Mark  |  25 | Rich-Mond |  65000
  5 | David |  27 | Texas     |  85000
  6 | Kim   |  22 | South-Hall|  45000
  7 | James |  24 | Houston   |  10000
(7 rows)
 Now, based on the above table, suppose you want to fetch maximum value of SALARY, then you can do so by simply using the following command − 
testdb=# SELECT MAX(salary) FROM COMPANY;
 The above given PostgreSQL statement will produce the following result − 
  max
-------
 85000
(1 row)
 You can find all the records with maximum value for each name using the GROUP BY clause as follows − 
testdb=# SELECT id, name, MAX(salary) FROM COMPANY GROUP BY id, name;
 The above given PostgreSQL statement will produce the following result − 
 id | name  |  max
----+-------+-------
  4 | Mark  | 65000
  7 | James | 10000
  6 | Kim   | 45000
  3 | Teddy | 20000
  2 | Allen | 15000
  5 | David | 85000
  1 | Paul  | 20000
 You can use the MIN Function along with the MAX function to find out minimum value as well. Try out the following example − 
testdb=# SELECT MIN(salary), MAX(salary) max FROM company;
 The above given PostgreSQL statement will produce the following result − 
  min  |  max
-------+-------
 10000 | 85000
(1 row)
 postgresql_useful_functions.htm
