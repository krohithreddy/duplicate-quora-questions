Neo4j - Where Clause

https://www.tutorialspoint.com/neo4j/neo4j_where_clause.htm

Neo4j, Tutorial, Learning, Overview, Data Model, Environment Setup, Building Blocks, CQL Introduction, Creating Nodes, Creating a Relationship, Merge Command, Set, Delete, Remove, Foreach, Match, Optional Match, Where, Return, Order By, Limit, Skip, With, Unwind Clause, Count, String, Aggregation Function, Backup and Restore, Index, Create Unique Constraint, Drop Unique.

August-15-2017-02:34:07

Like SQL, Neo4j CQL has provided WHERE clause in CQL MATCH command to filter the results of a MATCH Query. Syntax Following is the syntax of the WHERE clause. 
MATCH (label)  
WHERE label.country = "property" 
RETURN label 
 Example Before proceeding with the example, create five nodes in the database as shown below. 
CREATE(Dhawan:player{name:"shikar Dhawan", YOB: 1985, runs:363, country: "India"}
CREATE(Jonathan:player{name:"Jonathan Trott", YOB:1981, runs:229, country:"South Africa"}
CREATE(Sangakkara:player{name:"Kumar Sangakkara", YOB:1977, runs:222, 
   country:"Srilanka"})
CREATE(Rohit:player{name:"Rohit Sharma", YOB: 1987, runs:177, country:"India"})
CREATE(Virat:player{name:"Virat Kohli", YOB: 1988, runs:176, country:"India"})
CREATE(Ind:Country {name: "India", result: "Winners"})
 Following is a sample Cypher Query which returns all the players (nodes) that belongs to the country India using WHERE clause. 
MATCH (player)  
WHERE player.country = "India" 
RETURN player 
 To execute the above query, carry out the following steps − Step 1 − Open the Neo4j desktop App and start the Neo4j Server. Open the built-in browser app of Neo4j using the URL http://localhost:7474/ as shown in the following screenshot. Step 2 − Copy and paste the desired query in the dollar prompt and press the play button (to execute the query) highlighted in the following screenshot. Result On executing, you will get the following result. WHERE Clause with Multiple Conditions You can also use the WHERE clause to verify multiple conditions. Syntax Following is the syntax to use WHERE clause in Neo4j with multiple conditions. 
MATCH (emp:Employee)  
WHERE emp.name = 'Abc' AND emp.name = 'Xyz' 
RETURN emp 
 Example Following is a sample Cypher Query which filters the nodes in the Neo4j database using two conditions. 
MATCH (player)  
WHERE player.country = "India" AND player.runs >=175 
RETURN player 
 To execute the above query, carry out the following steps − Step 1 − Open the Neo4j desktop App and start the Neo4j Server. Open the built-in browser app of Neo4j using the URL http://localhost:7474/ as shown in the following screenshot. Step 2 − Copy and paste the desired query in the dollar prompt and press the play button (to execute the query) highlighted in the following screenshot. Result On executing, you will get the following result. Using Relationship with Where Clause You can also use Where clause to filter the nodes using the relationships. Example Assume we have the following graph in the database. Following is a sample Cypher Query to retrieve the top scorer of India using WHERE clause as shown below. 
MATCH (n) 
WHERE (n)-[: TOP_SCORER_OF]->( {name: "India", result: "Winners"}) 
RETURN n 
 To execute the above query, carry out the following steps − Step 1 − Open the Neo4j desktop App and start the Neo4j Server. Open the built-in browser app of Neo4j using the URL http://localhost:7474/ as shown in the following screenshot. Step 2 − Copy and paste the desired query in the dollar prompt and press the play button (to execute the query) highlighted in the following screenshot. Result On executing, you will get the following result. Here you can observe that Neo4j returned the node, which has the relation TOP_SCORER_OF to the country with the node having the name India.
