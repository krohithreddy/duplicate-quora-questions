MongoDB - Analyzing Queries

https://www.tutorialspoint.com/mongodb/mongodb_analyzing_queries.htm

Mongodb, overview, Advantages, Environment, Data Modelling, Create Database, Drop Database, Create Collection, Drop Collection, Data Types, Insert Document, Query Document, Update Document, Delete Document, Projection, Limiting Records, Sorting, Records, Indexing, Aggregration, Replication, Sharding, Create Backup, Deployment, Java, PHP, Relationships, Database References, Covered Queries, Analyzing Queries, Atomic Operations, Advanced Indexing, Indexing Limitations, ObjectId, Map Reduce, Text Search, Regular Expression, Rockmongo, GridFS, Capped Collections, Auto-Increment Sequence.

August-15-2017-02:34:09

Analyzing queries is a very important aspect of measuring how effective the database and indexing design is. We will learn about the frequently used $explain and $hint queries. Using $explain The $explain operator provides information on the query, indexes used in a query and other statistics. It is very useful when analyzing how well your indexes are optimized. In the last chapter, we had already created an index for the users collection on fields gender and user_name using the following query − 
>db.users.ensureIndex({gender:1,user_name:1})
 We will now use $explain on the following query − 
>db.users.find({gender:"M"},{user_name:1,_id:0}).explain()
 The above explain() query returns the following analyzed result − 
{
   "cursor" : "BtreeCursor gender_1_user_name_1",
   "isMultiKey" : false,
   "n" : 1,
   "nscannedObjects" : 0,
   "nscanned" : 1,
   "nscannedObjectsAllPlans" : 0,
   "nscannedAllPlans" : 1,
   "scanAndOrder" : false,
   "indexOnly" : true,
   "nYields" : 0,
   "nChunkSkips" : 0,
   "millis" : 0,
   "indexBounds" : {
      "gender" : [
         [
            "M",
            "M"
         ]
      ],
      "user_name" : [
         [
            {
               "$minElement" : 1
            },
            {
               "$maxElement" : 1
            }
         ]
      ]
   }
}
 We will now look at the fields in this result set − The true value of indexOnly indicates that this query has used indexing. The cursor field specifies the type of cursor used. BTreeCursor type indicates that an index was used and also gives the name of the index used. BasicCursor indicates that a full scan was made without using any indexes. n indicates the number of documents matching returned. nscannedObjects indicates the total number of documents scanned. nscanned indicates the total number of documents or index entries scanned. Using $hint The $hint operator forces the query optimizer to use the specified index to run a query. This is particularly useful when you want to test performance of a query with different indexes. For example, the following query specifies the index on fields gender and user_name to be used for this query − 
>db.users.find({gender:"M"},{user_name:1,_id:0}).hint({gender:1,user_name:1})
 To analyze the above query using $explain − 
>db.users.find({gender:"M"},{user_name:1,_id:0}).hint({gender:1,user_name:1}).explain()
