OrientDB - Connect Database

https://www.tutorialspoint.com/orientdb/orientdb_connect_database.htm

OrientDB, Tutorial, Learning, Beginners, Overview, Installation, Basic Concepts, Data Types, Console Modes, Create Database, Alter Database, Backup Database, Restore Database, Connect Database, Disconnect Database, Info Database, List Database, Freeze Database, Release Database, Config Database, Export Database, Import Database, Commit Database, Rollback Database, Optimize Database, Drop Database, Insert Record, Display Records, Load Record, Reload Record, Export Record, Update Record, Truncate Record, Delete Record, Create Class, Alter Class, Truncate Class, Drop Class, Create Cluster, Alter Cluster, Truncate Cluster, Drop Cluster, Create Property, Alter Property, Drop Property, Create Vertex, Move Vertex, Delete Vertex, Create Edge, Update Edge, Delete Edge, Functions, Sequences, Indexes, Transactions, Hooks, Caching, Logging, Performance Tuning, Upgrading, Security, Studio, Java Interface, Python Interface.

August-15-2017-02:34:10

This chapter explains how to connect to a particular database from the OrientDB command line. It opens a database. The following statement is the basic syntax of the Connect command. 
CONNECT <database-url> <user> <password>
 Following are the details about the options in the above syntax. <database-url> − Defines the URL of the database. URL contains two parts one is <mode> and the second one is <path>. <mode> − Defines the mode, i.e. local mode or remote mode. <path> − Defines the path to the database. <user> − Defines the user you want to connect to the database. <password> − Defines the password for connecting to the database. Example We have already created a database named ‘demo’ in the previous chapters. In this example, we will connect to that using the user admin. You can use the following command to connect to demo database. 
orientdb> CONNECT PLOCAL:/opt/orientdb/databases/demo admin admin
 If it is successfully connected, you will get the following output − 
Connecting to database [plocal:/opt/orientdb/databases/demo] with user 'admin'…OK 
Orientdb {db = demo}>
