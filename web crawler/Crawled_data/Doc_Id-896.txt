DOM - Node Object Method - isEqualNode

https://www.tutorialspoint.com/dom/dom_node_isequalnode.htm

XML, DOM, Document, Object, Model, simple, easy, steps, Overview, DOM Model, DOM Nodes, DOM Node Tree, DOM Node List, DOM Methods, DOM Loading, DOM Parser, DOM Traversing, DOM Navigation, DOM Accessing, Get, Set, Create, Add, Replace, Remove and Clone Node, Node Object, NodeList Object, NamedNodeMap Object, Document Object, DocumentImplementation Object, DocumentType Object, ProcessingInstruction Object, Entity Object, EntityReference Object, Notation Object, Element Object, Attribute Object, Text Object, CDATASection Object, Comment Object, XMLHttpRequest Object, ParseError Object, DOMException Object.

August-15-2017-02:34:12

The method isEqualNode tests whether two nodes are equal. Returns true if the nodes are equal, false otherwise. Syntax Following is the syntax for the usage of the isEqualNode method. 
nodeObject.isEqualNode(Node arg)
 S.No. Parameter & Description 1 arg It is the node with which equality condition is evaluated. It is of type Node. This method returns the boolean true if nodes are equal, false otherwise. Example node.xml contents are as below − 
<?xml version = "1.0"?>
<Company>
   <Employee category = "Technical">
      <FirstName>Tanmay</FirstName>
      <LastName>Patil</LastName>
      <ContactNo>1234567890</ContactNo>
      <Email>tanmaypatil@xyz.com</Email>
   </Employee>
   
   <Employee category = "Non-Technical">
      <FirstName>Taniya</FirstName>
      <LastName>Mishra</LastName>
      <ContactNo>1234667898</ContactNo>
      <Email>taniyamishra@xyz.com</Email>
   </Employee>
   
   <Employee category = "Management">
      <FirstName>Tanisha</FirstName>
      <LastName>Sharma</LastName>
      <ContactNo>1234562350</ContactNo>
      <Email>tanishasharma@xyz.com</Email>
   </Employee>
</Company>
 Following example demonstrates the usage of the isEqualNode method − 
<!DOCTYPE html>
<html>
   <head>
      <script>
         function loadXMLDoc(filename) {
            if (window.XMLHttpRequest) {
               xhttp = new XMLHttpRequest();
            } else // code for IE5 and IE6 {
               xhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xhttp.open("GET",filename,false);
            xhttp.send();
            return xhttp.responseXML;
         }
      </script>
   </head>
   <body>
      <script>
         xmlDoc = loadXMLDoc("/dom/node.xml");

         e1 = xmlDoc.getElementsByTagName("Employee")[1];
         e2 = xmlDoc.getElementsByTagName("Employee")[2];
         document.write("Checks the equality result : ")
         document.write(e1.isEqualNode(e2));
      </script>
   </body>
</html>
 Execution Save this file as nodemethod_isequalnode.htm on the server path (this file and node.xml should be on the same path in your server). We will get the output as shown below − 
Checks the equality result : false
 dom_node_object.htm
