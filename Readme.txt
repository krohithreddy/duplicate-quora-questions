					Duplicate Quora Question Pair Detection
						Group - 9

K.Rohith Reddy  cs14btech11018
P.Deepika       cs14btech11027
S.Kumuda Priya  cs14btech11029

Need Not download the dataset : Project code already has code to download the dataset and run the dataset.

Model1 : Using Sentence to Vector conversion
	 Main File : p1.py
	 python version : python3.5
	 Instructions to run : python p1.py
	 
Model2 : Using LSTM 
	Main File :  model2_lstm.ipynb
	python version : python3.5
	libraries needed to be installed : keras
	Instruction : open jupyter notebook and run
	
	File would take a lot of time in running , so jupyter notebook is submitted with graphs and results.
	
Improving the paper :

Model3: Stacking LSTM with neural networks.
	Main File : model3_improvement.ipynb
	python version : python3.5
	libraries needed to be installed : keras
	Instruction : open jupyter notebook and run
	
	File would take a lot of time in running , so jupyter notebook is submitted with graphs and results.

All the references to the implementation are mentioned in the ppt.
